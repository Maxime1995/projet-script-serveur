<?php

$output = '<h1 class="display-3 d-flex justify-content-center mt-4">Liste des Utilisateurs</h1>
           <div class="row w-50 mx-auto py-2">
           <table class="table table-light table-striped table-hover">
               <thead>
                 <tr>
                     <th scope="col">Id</th>
                     <th scope="col">Identifiant</th>
                     <th scope="col">Email</th>
                     <th scope="col">Création</th>
                     <th scope="col">Dernière Visite</th>
                     <th scope="col">Admin</th>
                 </tr>
               </thead>
               <tbody>';

$userList = getAllUsers();

  foreach($userList as $user) {
    $output .= '<tr>
                    <td>' . $user['id'] . '</td>
                    <td>' . $user['username'] . '</td>
                    <td>' . $user['email'] . '</td>
                    <td>' . date_format( new DateTime($user['created']),"d/m/Y H\hi") . '</td>
                    <td>' . date_format( new DateTime($user['lastlogin']),"d/m/Y H\hi") . '</td>
                    <td>' . getAdminStatus($user['admin']) . '</td>
                </tr>';
  }
$output .= '</tbody>';

echo $output;






