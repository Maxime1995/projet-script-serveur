<?php
if (function_exists('checkUrl') && function_exists('logout')) {
  logout();
  checkUrl();
} else {
  header('Location: index.php');
  die;
}
