<?php
$go_out = false;

if (!empty($_SESSION['userid'])) {
  $user = getUser('id', $_SESSION['userid']);
} else {
  $go_out = true;
}

if ($go_out) {
  header('Location: index.php?page=view/login');
  die;
}

$output = '<h1 class="display-3 d-flex justify-content-center mt-4">Profil</h1>
           <div class="container d-flex justify-content-start w-50 py-2">
                <a class="btn btn-outline-primary" href="index.php?page=app/export" role="button">Export JSON</a> 
           </div>
           <div class="row w-50 mx-auto py-2">
           <table class="table table-light table-striped table-hover">
               <thead>
                 <tr>
                     <th scope="col">Intitulé</th>
                     <th scope="col">Valeur</th>
                 </tr>
               </thead>
               <tbody>';

if (!empty($user)) {
  foreach ($user as $key => $value) {
    if ($key == 'admin') {
      $value = getAdminStatus($value);
    } elseif ($key == 'password' || $key == 'id') {
      continue;
    } elseif ($key == 'lastlogin' || $key == 'created') {
      $value = date_format( new DateTime($value),"d/m/Y H\hi");
    }

    if($key == 'username') {
      $output .= '<tr><th>Identifiant</th><td>' . htmlspecialchars($value) . '</td></tr>';
    } elseif ($key == 'created') {
      $output .= '<tr><th>Création</th><td>' . $value . '</td></tr>';
    } elseif ($key == 'lastlogin') {
      $output .= '<tr><th>Dernière visite</th><td>' . $value . '</td></tr>';
    } elseif($key == 'image') {
      continue;
    } else {
      $output .= '<tr><th>' . ucfirst($key) . '</th><td>' . htmlspecialchars($value) . '</td></tr>';
    }
  }
}

$output .= '</tbody></table></div>';

echo $output;

include_once 'update.php';




