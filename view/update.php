<?php

if (!empty($user)) {

  $output = '<div class="container-fluid my-4">
                <h1 class="display-3 d-flex justify-content-center my-4">Mise à jour du profil</h1>
                <div class="container d-flex justify-content-center">
                    <div class="accordion w-75" id="accordion">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  Mettre à jour votre adresse email et/ou votre mot de passe
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordion">
                                <div class="accordion-body d-flex justify-content-center">
                                    <form action="index.php?page=app/update" method="post" class="w-50">
                                        <input type="hidden" id="uu-userid" name="id" value="' . $user->id . '">
                                        <div class="mb-3">
                                          <label for="uu-email" class="form-label">Nouvelle adresse mail</label>
                                          <input type="email" class="form-control form-outline" name="email" id="uu-email" value="' . $user->email . '">
                                        </div>
                                        <div class="mb-3">
                                          <label for="uu-pwd" class="form-label">Nouveau mot de passe</label>
                                          <input type="password" class="form-control form-outline" name="password" id="uu-pwd">
                                        </div>
                                        <div class="d-grid gap-1 mx-auto p-2">
                                           <button type="submit" class="btn btn-primary">Mettre à jour</button> 
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
  echo $output;
}
