<nav class="navbar navbar-expand-md bg-dark">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-flex justify-content-center" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-info" href="index.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-info" href="index.php?page=view/courses">Liste des cours</a>
        </li>
        <?php
            if (!empty($_SESSION['userid'])) {
        ?>
        <li class="nav-item">
          <a class="nav-link text-info" href="index.php?page=view/profile">Profil</a>
        </li>
        <?php
            }
            if(!empty($_SESSION['userid'])) {
                $user = getUser('id', $_SESSION['userid']);
                if($user->admin == 1) {
        ?>
                    <li class="nav-item">
                        <a class="nav-link text-info" href="index.php?page=view/admin">Admin</a>
                    </li>
        <?php
                }
            }
        ?>
        <?php
            if (!empty($_SESSION['userid'])) {
        ?>
        <li class="nav-item">
           <a class="nav-link text-info" href="index.php?page=view/logout">Logout</a>
        </li>
        <?php
            } else {
        ?>
        <li class="nav-item">
          <a class="nav-link text-info" href="index.php?page=view/login">Log In</a>
        </li>
        <?php
            }
        ?>
      </ul>
    </div>
  </div>
</nav>