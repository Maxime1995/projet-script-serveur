<?php
/**
 * @return array
 */
function getAllCourseDatas():array
{
  global $connect;

  $data = $connect->prepare("SELECT * FROM course ORDER BY name ASC");

  $data->execute();

  return $data->fetchAll();
}
