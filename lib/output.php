<?php
/**
 * @param string $content
 * @return void
 */
function getContent(string $content):void
{
  if (is_array(FILE_EXT)) {
    foreach (FILE_EXT as $extension) {
      $filename = $content . '.' . $extension;
      if (file_exists($filename)) {
        include_once $filename;
      }
    }
  }
}

function getAdminStatus(int $adminStatus): string
{
  if ($adminStatus == 1) {
    return 'Oui';
  } else {
    return 'Non';
  }
}

/**
 * @param array $courseData
 * @return void
 */
function displayCourseList(array $courseData):void {

  $output = '<h1 class="display-3 row d-flex justify-content-center">Liste des cours</h1>
             <div class="row w-75 mx-auto">
             <table class="table table-light table-striped table-hover">
              <thead>
                <tr>
                    <th scope="col">Nom du cours</th>
                    <th scope="col">Code du cours</th>
                </tr>
              </thead>
              <tbody>';

  foreach ($courseData as $course) {
    $output .= '<tr><td>'. $course['name'] . '</td>' . '<td>'. $course['code'] . '</td></tr>';
  }

  $output .= '</tbody></table></div>';

  echo $output;
}

/**
 * @param string $type
 * @param string $message
 * @return void
 */
function displayAlert(string $type, string $message):void {

  $output = '<div class="alert alert-' . $type . ' d-flex align-items-center">';

  switch ($type) {
    case 'success':
      $output .= '<svg class="bi flex-shrink-0 me-2" width="16" height="16" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/>';
      break;
    case 'info':
      $output .= '<svg class="bi flex-shrink-0 me-2" width="16" height="16" role="img" aria-label="Info:"><use xlink:href="#info-fill"/>';
      break;
    default:
      $output .= '<svg class="bi flex-shrink-0 me-2" width="16" height="16" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/>';
  }

  $output .= '<div>' . $message . '</div></div>';

  echo $output;
}
