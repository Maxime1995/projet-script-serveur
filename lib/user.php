<?php

/**
 * @param string $field
 * @param string $value
 * @return mixed
 */
function getUser(string $field, string $value): mixed
{
  if (!in_array($field, getColumns('user'))) {
    return false;
  }

  $connect = connect();

  $request = $connect->prepare("SELECT * FROM user WHERE $field = ?");

  $params = [
    trim($value),
  ];

  $request->execute($params);

  return $request->fetchObject();
}

/**
 * @param string $field
 * @param string $value
 * @return bool
 */
function userExists(string $field, string $value): bool
{
  if (is_object(getUser($field, $value))) {
    return true;
  } else {
    return false;
  }
}

/**
 * @return void
 */
function logout(): void
{
  session_unset();
  $_SESSION['alert-color'] = 'info';
  $_SESSION['alert'] = 'Au revoir';
  header('Location: index.php');
  die;
}

/**
 * @return bool
 */
function checkUserAdminExists(): bool {
  $connect = connect();

  $query = $connect->prepare("SELECT COUNT(*) FROM user WHERE admin = 1");
  $query->execute();
  $adminCount = $query->fetchColumn();

  if ($adminCount == 1) {
    return true;
  } else {
    return false;
  }
}

/**
 * @param string $table
 * @param array $fields
 * @param string $whereCondition
 * @return string
 */
function getUpdateQuery(string $table, array $fields, string $whereCondition):string
{
  $query = "UPDATE " . $table . " SET ";
  $fieldsToUpdate = array();

  foreach ($fields as $key => $value) {
    if(!empty($value) && !empty($key)) {
      $fieldsToUpdate[] = $key . ' = ?';
    }
  }

  $query .= implode(", ", $fieldsToUpdate) . " WHERE " . $whereCondition;

  return $query;
}

/**
 * @param array $params
 * @return array
 */
function getUpdateParams(array $params):array {
  foreach ($params as $key => $value) {
    if ($value == null) {
      unset($params[$key]);
    }
  }

  return $params;
}

/**
 * @return array|false
 */
function getAllUsers():array|false
{
  $connect = connect();

  $query = $connect->prepare("SELECT * FROM user");

  $query->execute();

  if (!empty($query)) {
    return $query->fetchAll(PDO::FETCH_ASSOC);
  } else {
    return false;
  }
}

