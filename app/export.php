<?php
$user = getUser('id', $_SESSION['userid']);

if(!empty($user)) {
    ob_clean();

    $filename = $user->username . '_' . time() . '.json';
    unset($user->password);
    header('Content-disposition: attachment; filename="' . $filename . '"');
    header('Content-Type: application/json');
    echo json_encode($user);
    die;
} else {
    $_SESSION['alert'] = 'Une erreur s\'est produite lors de l\'export de vos données au format JSON : Vous n\'êtes pas pas connecté ou autorisé à obtenir ces données';
}





