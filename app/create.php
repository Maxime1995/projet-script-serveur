<?php
$url = 'index.php?page=view/create';
if (!empty($_POST['username']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

  if (userExists('username', $_POST['username'])) {
    $_SESSION['alert'] = 'L\'utilisateur existe déjà!';
    header('Location: ' . $url);
    die;
  }

  $username = preg_replace('/[^A-Za-z0-9]/', '', $_POST['username']);

  if(empty($username)) {
    $_SESSION['alert'] = 'Nom d\'utilisateur invalide !';
    header('Location: ' . $url);
    die;
  }

  if (is_object(getUser('email', $_POST['email']))) {
    $_SESSION['alert'] = 'Cet email existe déjà !';
    header('Location: ' . $url);
    die;
  }

  $params = [
    $username,
    $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL),
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT)
  ];

  $connect = connect();

  $insert = $connect->prepare("INSERT INTO user (username, email, password, created, lastlogin, admin) VALUES (?, ? ,?, NOW(), NOW(), 0)");

  $insert->execute($params);

  $lastInsertedId = $connect->lastInsertId();

  if ($insert->rowCount()) {

    if(!checkUserAdminExists()) {
      $sql = 'UPDATE user SET admin = 1 WHERE id = ?';
      $update = $connect->prepare($sql);
      $update->execute([$lastInsertedId]);
    }

    $_SESSION['alert'] = 'Utilisateur ' . $username . ' a été créé avec succès';
    $_SESSION['alert-color'] = 'success';
    $url = 'index.php?page=view/login';
  } else {
    $_SESSION['alert'] = 'La création a échoué';
  }


} else {
  $_SESSION['alert'] = 'Un ou plusieurs champs vide(s) détecté(s)';
}
header('Location: ' . $url);
die;
