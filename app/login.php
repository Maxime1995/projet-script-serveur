<?php
$url = 'index.php?page=view/login';
if (empty($_SESSION['userid'])) {

  if(!empty($_POST['username']) && !empty($_POST['password'])) {

    $username = trim($_POST['username']);

    $user = getUser('username', $_POST['username']);

    if(!empty($user) && password_verify($_POST['password'], $user->password)) {
      if(!empty($user->id)) {
          $_SESSION['userid'] = $user->id;

          $sql = "UPDATE user SET lastlogin = NOW() WHERE id = ?";

          $connect = connect();

          $update = $connect->prepare($sql);

          $update->execute([$user->id]);

          if($update->rowCount()) {
              $_SESSION['alert'] = 'Bienvenue ' . $user->username;
              $_SESSION['alert-color'] = 'success';
          }
          $url = 'index.php?page=view/profile';
      }
    } else {
      $_SESSION['alert'] = 'Echec de l\'authentification : mot de passe ou nom d\'utilisateur incorrect';
    }

  } else {
    $_SESSION['alert'] = 'Echec de l\'authentification : un ou plusieurs champs vide(s) détecté(s)';
  }
} else {
  header('Location: index.php');
  die;
}
header('Location: ' . $url);
die;
